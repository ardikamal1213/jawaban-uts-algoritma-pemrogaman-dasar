#include <iostream>

using namespace std;

void cetakBintang(int n) {

  // Looping baris atas
  for(int i=1; i<=n; i++) {

    // Cetak spasi kiri
    for(int j=1; j<=n-i; j++) {
      cout << " ";
    }
    
    // Cetak bintang kiri
    for(int k=1; k<=i; k++) {
      cout << "*";
    }

    // Cetak spasi tengah
    for(int l=1; l<=n-i; l++) {
      cout << " ";
    }

    // Cetak bintang kanan  
    for(int m=1; m<=i; m++) {
      cout << "*";
    }
    
    cout << endl;
  }
  
  // Looping baris bawah
  for(int i=n-1; i>=1; i--) {

    // Cetak spasi kiri
    for(int j=1; j<=n-i; j++) {
      cout << " ";
    }
    
    // Cetak bintang kiri  
    for(int k=1; k<=i; k++) {
      cout << "*";
    }

    // Cetak spasi tengah
    for(int l=1; l<=n-i; l++) {  
      cout << " ";
    }

    // Cetak bintang kanan
    for(int m=1; m<=i; m++) {
      cout << "*";
    }

    cout << endl;
  }
  
}

int main() {
  int n;
  cin >> n;

  cetakBintang(n);
  
  return 0;  
}