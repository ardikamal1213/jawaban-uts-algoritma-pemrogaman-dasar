#include <iostream>
#include <iomanip>

using namespace std;

void konversiSuhu() {

  double celcius, fahrenheit, kelvin;
  int awal, akhir, step;

  cout << "Masukan suhu awal   = ";
  cin >> awal;
  cout << "Masukan step        = "; 
  cin >> step;
  cout << "Masukan suhu akhir  = ";
  cin >> akhir;
  cout << endl;
  
  // Print table header
  cout << setw(10) << "Celcius";
  cout << setw(12) << "Fahrenheit";
  cout << setw(10) << "Kelvin" << endl;  

  for(int i=awal; i<=akhir; i+=step) {

    celcius = i;
    fahrenheit = celcius * 1.8 + 32;
    kelvin = celcius + 273.15;

    cout << fixed << setprecision(2);
    cout << setw(10) << celcius;
    cout << setw(12) << fahrenheit;
    cout << setw(10) << kelvin << endl;

  }

}

int main() {

  konversiSuhu();
  
  return 0;

}